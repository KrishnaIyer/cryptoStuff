/**
* Ascii<-->Hex libraries
*
* @author  Krishna Iyer
* @since   2 Feb 2018
* 
*/

/**************************************
            Header Definitions
***************************************/
#include "lib_AsciiHex.h"


/**************************************
            Public Functions
***************************************/
 BOOL Lib_getHexForAscii(char ascii, UINT8 *retHexValue)
 {
        sscanf(ascii, "%x", *retHexValue);
 }